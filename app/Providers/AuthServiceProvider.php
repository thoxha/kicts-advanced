<?php

namespace App\Providers;

use App\Models\Article;
use App\Policies\ArticlePolicy;
use Illuminate\Auth\Access\Response;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        Article::class => ArticlePolicy::class,
    ];


    public function boot()
    {
        $this->registerPolicies();



        Gate::define('delete-article', function ($user, $article, $days = 10) {
            $post_age_days = (time()  - strtotime($article->created_at)) / 86400;
            return ($post_age_days >= $days && $article->user_id == $user->id);
        });

        Gate::define('edit-article', function ($user, $article, $days = 10) {
            $post_age_days = (time()  - strtotime($article->created_at)) / 86400;
            return ($post_age_days <= $days && $article->user_id == $user->id);
        });

        Gate::define('update-article', function ($user, $article) {
            return ($user->role == "admin" && $article->user_id == $user->id)
                ? Response::allow()
                : Response::deny('Ju duhet te autentikoheni si administrator ose postimi nuk eshte i juaji.');
        });

    }
}
