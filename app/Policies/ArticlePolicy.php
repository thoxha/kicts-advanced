<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Article $article, $days = 10) {
        $post_age_days = (time()  - strtotime($article->created_at)) / 86400;
        return ($post_age_days <= $days && $article->user_id == $user->id);
    }
}
