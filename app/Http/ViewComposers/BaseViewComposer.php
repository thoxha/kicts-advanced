<?php

namespace App\Http\ViewComposers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class BaseViewComposer
{


    public function compose(View $view)
    {
        $data = $view->getData();

        $data['articlesCount'] = Article::count();
        $data['usersCount'] = User::count();
        $data['lastArticle'] = Article::orderBy('id', 'desc')->first();

        $view->with($data);
    }
}
