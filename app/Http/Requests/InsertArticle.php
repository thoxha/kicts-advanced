<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsertArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $this->sanitize();

        return [
            'article_name' => "required|string|min:5|max:15",
            'barcode' => "required|integer|min:0|max:9999999999999",
            'description' => "nullable",
            'price' => "required|numeric",
            'category_id' => "required|exists:categories,id",
        ];
    }

    public function sanitize() {
        $input = $this->all();
        $input['article_name'] =  filter_var($input['article_name'], FILTER_SANITIZE_STRING);
        $input['barcode']= filter_var( $input['barcode'], FILTER_SANITIZE_NUMBER_INT);
        $input['description'] =  filter_var($input['description'], FILTER_SANITIZE_STRING);
        $input['price'] = filter_var($input['price'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $input['category_id'] = filter_var($input['category_id'], FILTER_SANITIZE_NUMBER_INT);
        $this->replace($input);
    }
}
