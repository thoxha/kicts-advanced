<?php

namespace App\Http\Controllers;

use App\Http\Requests\InsertArticle;
use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        return view('articles', compact('articles'));
    }

    public function article(Article $article)
    {
        return view('article', compact('article'));

    }

    public function el()
    {
        $articles = Article::
        orderBy('article_name', 'asc')
            ->where('price', '<', 10)
            ->get();


        if (!$articles->isEmpty()) {
            foreach ($articles as $article) {
                echo "<p>" . $article->article_name . " = " . $article->price . "</p>";
            }
        } else {
            echo "Nuk ka rezultate";
        }
    }

    public function el2($id)
    {
        $article = Article::find($id);
        if ($article != null) {
            echo $article->price;
        } else {
            echo "Nuk ka produkt me ID-ne e kerkuar";
        }
    }

    public function el2b($id)
    {
        $article = Article::findOrFail($id);
        echo $article->price;
    }

    public function el3($barcode)
    {
        $article = Article::where('barcode', '=', $barcode)->first();
        dump($article->price);
    }

    public function el4($category_id)
    {
        $article = Article::orderBy('id', 'desc')
            ->where('category_id', '=', $category_id)->first();
        dump($article->id);
    }

    public function productsByCategory($category_id)
    {
        $category = Category::findOrFail($category_id);
        echo "<h2>" . $category->category_name . "</h2>";

        foreach ($category->articles as $article) {
            echo "<p>" . $article->article_name . " -> " . $article->user->name . "</p>";
        }
    }

    public function productsByUser($user_id)
    {
        $user = User::findOrFail($user_id);
        echo "<h2>" . $user->name . "</h2>";

        foreach ($user->articles as $article) {
            echo "<p>" . $article->article_name . " -> " . $article->category->category_name . "</p>";
        }
    }

    public function editArticle($id = 0)
    {
        $categories = Category::orderBy('category_name')->get();
        if ($id > 0) {
            $article = Article::findOrFail($id);
            if (Gate::allows('edit-article', [$article, 10])) {
                return view('edit_article', compact('categories', 'article', 'id'));
            } else {
                echo "Ju nuk jeni te autorizuar per kete veprim!";
            }

        } else {
            $article = (object)[
                'id' => null,
                'article_name' => '',
                'barcode' => '',
                'price' => 0,
                'category_id' => 0,
                'description' => ''
            ];
            return view('edit_article', compact('categories', 'article', 'id'));
        }


    }

    public function updateArticle($id = 0)
    {

        $article = Article::findOrFail($id);
        $response = Gate::inspect('update-article', $article);

        if ($response->allowed()) {
            echo "Perditesimi u kry me sukses!";
        } else {
            echo $response->message();
        }

    }

    public function deleteArticle($id = 0)
    {
        $article = Article::findOrFail($id);
        Gate::authorize('delete-article', [$article, 10]);

        $article->delete();
        echo "Postimi $id u fshi";

    }


    public function insertProduct(InsertArticle $request)
    {
        $article_id = filter_var($request->input('article_id'), FILTER_SANITIZE_NUMBER_INT);

        if ($article_id == 0) {
            $article = new Article();
        } else {
            $article = Article::find($article_id);
        }

        $article->article_name = $request->input('article_name');
        $article->article_slug = Str::slug($request->input('article_name'));
        $article->barcode = $request->input('barcode');
        $article->description = $request->input('description');
        $article->price = $request->input('price');
        $article->category_id = $request->input('category_id');
        $article->user_id = Auth::id();

        if ($article->save()) {
            echo "<p>Produkti u regjistrua/perditesua me sukses!</p>";
        } else {
            echo "<p>Produkti NUK u regjistrua/perditesua!</p>";
        }
    }


    public function korigjoArtikullin($id = 0)
    {
       $article = Article::findOrFail($id);
        if (Auth::user()->can('edit', $article)) {
            echo "lejohet";
        } else {
            echo "Nuk lejohet";
        }


    }
}
