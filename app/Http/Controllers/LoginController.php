<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function form() {
        return view('loginform');
    }

    public function login(Request $request) {
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard');
        } else {
            return redirect()->intended('loginform');
        }

    }

    public function login2(Request $request) {

          $username =  $request->input('username');
          $password = $request->input('password');

            $user = User::where('username', $username)->first();
            if ($user !=null) {
                if (password_verify($password, $user->password)) {
                    Auth::login($user);
                    return redirect()->intended('dashboard');

                } else {
                    echo "Ju nuk jeni autentikuar";
                }
            } else {
                echo "Nuk ekziston ku user";
            }

    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
