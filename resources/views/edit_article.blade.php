@extends('layouts.base')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<form method="post" action="{{ route('insertProduct') }}">
    @csrf

    <input type="hidden" name="article_id" value="{{ $id }}">
    <div>
        <label for="article_name">Emri i artikullit</label>
        <input type="text" id="article_name" name="article_name" class="form-control"
               value="{{ old('article_name', $article->article_name) }}">
    </div>

    <div>
        <label for="barcode">Barkodi</label>
        <input type="text" id="barcode" name="barcode" value="{{ old('barcode', $article->barcode) }}" class="form-control">
    </div>

    <div>
        <label for="description">Pershkrimi</label>
        <textarea name="description" id="description" rows="5" class="form-control">{{ old('description',$article->description) }}</textarea>
    </div>

    <div>
        <label for="price">Cmimi</label>
        <input type="text" id="price" name="price" value="{{ old('price', $article->price) }}" class="form-control">
    </div>

    <div>
        <label for="category_id">Kategoria</label>
        <select id="category_id" name="category_id"class="form-control">
            @foreach($categories as $category)
                @if ($category->id == old('category_id', $article->category_id))
                    <option selected value="{{ $category->id }}">{{ $category->category_name }}</option>
                @else
                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                @endif
            @endforeach
        </select>
    </div>

    <input type="submit" value="REGJISTROJE" class="btn btn-primary">

</form>
@endsection


@section('sidebar')
    <h4>Sidebar</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, eaque enim eum in maiores minima molestiae mollitia natus obcaecati, praesentium quibusdam quisquam repellat reprehenderit saepe sapiente, ut voluptatem. Eveniet, explicabo.</p>
@endsection
