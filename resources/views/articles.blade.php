@extends('layouts.base')

@section('content')
<table border="1" cellpadding="3" cellspacing="1">
    @foreach($articles as $article)
        <tr>
            <td><a href="{{ route("article", [$article]) }}">{{ $article->article_name }}</a></td>
            <td>{{ $article->barcode }}</td>
            <td>{{ $article->price }}</td>
        </tr>
    @endforeach
</table>

    @include('partials.additional_info')
@endsection
