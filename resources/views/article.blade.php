@extends('layouts.base')

@section('content')
    <h1>{{ $article->article_name }}</h1>

    <table>
        <tr>
            <td>Barcode:</td>
            <td>{{ $article->barcode }}</td>
        </tr>
        <tr>
            <td>Price:</td>
            <td>{{ $article->price }}</td>
        </tr>
        <tr>
            <td>Create at:</td>
            <td>{{ date("d.m.Y", strtotime($article->created_at)) }}</td>
        </tr>
    </table>
@endsection
