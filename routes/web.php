<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::get('articles', [ArticleController::class, "index"])->name('articles');
Route::get('articles/{article}', [ArticleController::class, "article"])->name('article');
Route::get('el', [ArticleController::class, "el"])->name('el');
Route::get('el2/{id}', [ArticleController::class, "el2"])->name('el2');
Route::get('el2b/{id}', [ArticleController::class, "el2b"])->name('el2b');
Route::get('el3/{barcode}', [ArticleController::class, "el3"])->name('el3');
Route::get('el4/{category_id}', [ArticleController::class, "el4"])->name('el4');
Route::get('products_by_category/{category_id}', [ArticleController::class, "productsByCategory"])->name('productsByCategory');
Route::get('products_by_user/{user_id}', [ArticleController::class, "productsByUser"])->name('productsByUser');

Route::get('edit_article/{id?}', [ArticleController::class, 'editArticle'])->name('editArticle')->middleware('is_admin');
Route::get('korigjo_artikullin/{id?}', [ArticleController::class, 'korigjoArtikullin'])->name('korigjoArtikullin')->middleware('is_admin');
Route::get('update_article/{id?}', [ArticleController::class, 'updateArticle'])->name('updateArticle')->middleware('is_admin');
Route::get('delete_article/{id?}', [ArticleController::class, 'deleteArticle'])->name('deleteArticle')->middleware('is_admin');
Route::post('insert_product', [ArticleController::class, "insertProduct"])->name('insertProduct')->middleware('is_admin');

Route::get('loginform', [LoginController::class, 'form'])->name('loginform');
Route::post('customlogin', [LoginController::class, 'login'])->name('customlogin');
Route::post('customlogin2', [LoginController::class, 'login2'])->name('customlogin2');
Route::get('customlogout', [LoginController::class, 'logout'])->name('customlogout');
